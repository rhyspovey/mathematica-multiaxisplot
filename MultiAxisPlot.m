(* ::Package:: *)

(* ::Title:: *)
(*MultiAxisPlot*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Section:: *)
(*Front End*)


BeginPackage["MultiAxisPlot`"];


(* ::Subsection:: *)
(*New*)


TwoAxisListPlot::usage="TwoAxisListPlot[{{\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(1\)]\),\!\(\*SubscriptBox[
StyleBox[\"a\",\nFontSlant->\"Italic\"], \(1\)]\),\!\(\*SubscriptBox[
StyleBox[\"b\",\nFontSlant->\"Italic\"], \(1\)]\)},{\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(2\)]\),\!\(\*SubscriptBox[
StyleBox[\"a\",\nFontSlant->\"Italic\"], \(2\)]\),\!\(\*SubscriptBox[
StyleBox[\"b\",\nFontSlant->\"Italic\"], \(2\)]\)},\[Ellipsis]}] plots \!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\) horizontally, \!\(\*
StyleBox[\"a\",\nFontSlant->\"Italic\"]\) with the left axis, and \!\(\*
StyleBox[\"b\",\nFontSlant->\"Italic\"]\) with the right axis. Accepts most ListPlot options.";


TwoAxisListPlot::dims="Input data dimensions `` are not compatible with TwoAxisListPlot.";


TwoAxisPlot::usage="TwoAxisPlot[{\!\(\*SubscriptBox[
StyleBox[\"f\",\nFontSlant->\"Italic\"], \(1\)]\)[\!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\)],\!\(\*SubscriptBox[
StyleBox[\"f\",\nFontSlant->\"Italic\"], \(2\)]\)[\!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\)]},{\!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\),\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(min\)]\),\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(max\)]\)}] plots \!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\) horizontally, \!\(\*SubscriptBox[
StyleBox[\"f\",\nFontSlant->\"Italic\"], \(1\)]\) with the left axis, and \!\(\*SubscriptBox[
StyleBox[\"f\",\nFontSlant->\"Italic\"], \(\(2\)\(\\\ \)\)]\)with the right axis. Accepts most Plot options.";


ColumnListPlot::usage="ColumnListPlot[{{\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(1\)]\),\!\(\*SubscriptBox[
StyleBox[\"a\",\nFontSlant->\"Italic\"], \(1\)]\),\!\(\*SubscriptBox[
StyleBox[\"b\",\nFontSlant->\"Italic\"], \(1\)]\),\[Ellipsis]},{\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(2\)]\),\!\(\*SubscriptBox[
StyleBox[\"a\",\nFontSlant->\"Italic\"], \(2\)]\),\!\(\*SubscriptBox[
StyleBox[\"b\",\nFontSlant->\"Italic\"], \(2\)]\),\[Ellipsis]},\[Ellipsis]}] plots \!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\) horizontally and then each of \!\(\*
StyleBox[\"a\",\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[\"b\",\nFontSlant->\"Italic\"]\),\[Ellipsis] vertically in a column. Accepts most ListPlot options.";


ColumnPlot::usage="ColumnPlot[{\!\(\*SubscriptBox[
StyleBox[\"f\",\nFontSlant->\"Italic\"], \(1\)]\)[\!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\)],\!\(\*SubscriptBox[
StyleBox[\"f\",\nFontSlant->\"Italic\"], \(2\)]\)[\!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\)],\[Ellipsis]},{\!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\),\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(min\)]\),\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(max\)]\)}] plots \!\(\*
StyleBox[\"x\",\nFontSlant->\"Italic\"]\) horizontally and each of \!\(\*SubscriptBox[
StyleBox[\"f\",\nFontSlant->\"Italic\"], 
StyleBox[\"i\",\nFontSlant->\"Italic\"]]\) separetly in a column. Accepts most Plot options.";


(* ::Text:: *)
(*Backend but available for use*)


MultiAxisListPlotRanges::usage="MultiAxisPlotRangeOption[option,{{\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(a\), \(1\)]\),\!\(\*SubscriptBox[\(b\), \(1\)]\),\[Ellipsis]},{\!\(\*SubscriptBox[\(x\), \(2\)]\),\!\(\*SubscriptBox[\(a\), \(2\)]\),\!\(\*SubscriptBox[\(b\), \(2\)]\),\[Ellipsis]},\[Ellipsis]}] returns {horizplotrange, \!\(\*SubscriptBox[\(vertplotrange\), \(1\)]\), \!\(\*SubscriptBox[\(vertplotrange\), \(2\)]\), \[Ellipsis]}.";


MultiAxisPlotRanges::usage="MultiAxisPlotRanges[option,{\!\(\*SubscriptBox[\(f\), \(1\)]\)[x],\!\(\*SubscriptBox[\(f\), \(2\)]\)[x],\[Ellipsis]},{x,\!\(\*SubscriptBox[\(x\), \(min\)]\),\!\(\*SubscriptBox[\(x\), \(max\)]\)}]";


TwoAxisFrameTicks::usage="TwoAxisFrameTicks[option,{\!\(\*SubscriptBox[\(mapfunc\), \(1\)]\),\!\(\*SubscriptBox[\(mapfunc\), \(2\)]\),\[Ellipsis]},{\!\(\*SubscriptBox[\(vertplotrange\), \(1\)]\),\!\(\*SubscriptBox[\(vertplotrange\), \(2\)]\),\[Ellipsis]}]";


ColumnFrameTicks::usage="ColumnFrameTicks[option,{\!\(\*SubscriptBox[\(mapfunc\), \(1\)]\),\!\(\*SubscriptBox[\(mapfunc\), \(2\)]\),\[Ellipsis]},{\!\(\*SubscriptBox[\(vertplotrange\), \(1\)]\),\!\(\*SubscriptBox[\(vertplotrange\), \(2\)]\),\[Ellipsis]}]";


(* ::Subsection::Closed:: *)
(*Old*)


MultiAxisPlot::usage="MultiAxisPlot[\!\(\*
StyleBox[\"data\",\nFontSlant->\"Italic\"]\)] plots \!\(\*
StyleBox[\"data\",\nFontSlant->\"Italic\"]\) of the form {{\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(1\)]\),\!\(\*SubscriptBox[
StyleBox[\"a\",\nFontSlant->\"Italic\"], \(1\)]\),\!\(\*SubscriptBox[
StyleBox[\"b\",\nFontSlant->\"Italic\"], \(1\)]\),...},{\!\(\*SubscriptBox[
StyleBox[\"x\",\nFontSlant->\"Italic\"], \(2\)]\),\!\(\*SubscriptBox[
StyleBox[\"a\",\nFontSlant->\"Italic\"], \(2\)]\),\!\(\*SubscriptBox[
StyleBox[\"b\",\nFontSlant->\"Italic\"], \(2\)]\),...},...} giving \!\(\*
StyleBox[\"a\",\nFontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"b\",\nFontSlant->\"Italic\"]\), ... separate axes.";


(* ::Section:: *)
(*Back End*)


Begin["`Private`"];


(* ::Subsection:: *)
(*New*)


MultiAxisListPlotRanges[option_,input_List]:=Module[{n=Dimensions[input][[2]]-1},
Join[
	{If[ListQ[option]\[And](Length[option]==n+1)\[And]ListQ[option[[1]]],option[[1]],MinMax[input[[All,1]]]]},
	Table[
	If[ListQ[option],
		Switch[Length[option],
			n+1,option[[1+\[FormalI]]],
			n,option[[\[FormalI]]]
		],
		MinMax[input[[All,1+\[FormalI]]]]
	],
	{\[FormalI],n}]
]
];


MultiAxisPlotRanges[option_,funcs_List,vardomain_List]:=Module[{n=Length[funcs],o},
If[ListQ[option],
	Switch[Length[option],
		n+1,Do[o[\[FormalI]]=option[[1+\[FormalI]]],{\[FormalI],0,n}],
		n,o[0]=Rest@vardomain;Do[o[\[FormalI]]=option[[\[FormalI]]],{\[FormalI],1,n}],
		2,o[0]=option[[1]];Do[o[\[FormalI]]=option[[2]],{\[FormalI],1,n}],
		1,Do[o[\[FormalI]]=option[[1]],{\[FormalI],0,n}]
	]
];
Join[
{If[ListQ[o[0]],o[0],Rest@vardomain]},
Table[
	If[ListQ[o[\[FormalI]]],
		o[\[FormalI]],
		Lookup[AbsoluteOptions[Plot[funcs[[\[FormalI]]],vardomain,PlotRange->o[\[FormalI]]]],PlotRange][[2]]
	],{\[FormalI],n}]]
];


TwoAxisFrameTicks[option_,mapfunc_List,plotrange_List]:=Module[{frameticks},
(* frameticks[0] for bottom, frameticks[1] for left, frameticks[2] for right, frameticks[-1] for top *)
If[ListQ[option],
	(* list input*)
	If[ListQ[option[[1]]],
		Do[frameticks[\[FormalI]]=If[MatchQ[option[[1,\[FormalI]]],Automatic|All],
			MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],
			Switch[Depth[option[[1,\[FormalI]]]],
				1, option[[1,\[FormalI]]],
				2, {mapfunc[\[FormalI]][#],#}&/@option[[1,\[FormalI]]],
				3, {mapfunc[\[FormalI]][#[[1]]],#[[2]]}&/@option[[1,\[FormalI]]]
			]
		],{\[FormalI],2}],
		Switch[option[[1]],
			Automatic|All,Do[frameticks[\[FormalI]]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],2}],
			None|_,Do[frameticks[\[FormalI]]=None,{\[FormalI],2}]
		]
	];
	If[ListQ[option[[2]]],
		Do[frameticks[1-\[FormalJ]]=option[[2,\[FormalJ]]],{\[FormalJ],2}],
		Do[frameticks[1-\[FormalJ]]=option[[2]],{\[FormalJ],2}]
	],
	(* single input *)
	Switch[option,
		Automatic, Do[frameticks[\[FormalI]]=Automatic,{\[FormalI],-1,0}];Do[frameticks[\[FormalI]]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],2}],
		All, Do[frameticks[\[FormalI]]=All,{\[FormalI],-1,0}];Do[frameticks[\[FormalI]]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],2}],
		None|_, Do[frameticks[\[FormalI]]=None,{\[FormalI],-1,2}]
	]
];
(* output *)
{{frameticks[1],frameticks[2]},{frameticks[0],frameticks[-1]}}
]


TwoAxisFrameStyle::usage="TwoAxisFrameStyle[option,plotstyle list]";


TwoAxisFrameStyle[option_,plotstyle_List]:=Module[{framestyle},
(* framestyle[0] for bottom, framestyle[1] for left, framestyle[2] for right, framestyle[-1] for top *)
If[ListQ[option],
	If[Length[option]>0,
		If[ListQ[option[[1]]],
		Do[framestyle[\[FormalI]]=If[option[[1,\[FormalI]]]===Automatic,plotstyle[[\[FormalI]]],option[[1,\[FormalI]]]],{\[FormalI],2}],
		Do[framestyle[\[FormalI]]=If[option[[1]]===Automatic,plotstyle[[\[FormalI]]],option[[1]]],{\[FormalI],2}]
		];
		If[Length[option]>1,
		If[ListQ[option[[2]]],
		Do[framestyle[1-\[FormalJ]]=option[[2,\[FormalJ]]],{\[FormalJ],2}],
		Do[framestyle[1-\[FormalJ]]=option[[2]],{\[FormalJ],2}]
		],
		Do[framestyle[\[FormalI]]=Automatic,{\[FormalI],-1,0}]
		],
		(* default *)
		Do[framestyle[\[FormalI]]=Automatic,{\[FormalI],-1,0}];Do[framestyle[\[FormalI]]=plotstyle[[\[FormalI]]],{\[FormalI],2}]
	],
	(* single input *)
	Do[framestyle[\[FormalI]]=option,{\[FormalI],-1,2}]
];
(* output *)
{{framestyle[1],framestyle[2]},{framestyle[0],framestyle[-1]}}
]


TwoAxisListPlot[input_List,opts:OptionsPattern[]]:=Module[{
n=2,
plotranges,gfxrange,mapfunc,mappeddata,
framestyle,framelabel
},
If[Not@MatchQ[Dimensions[input],{_,3}],Message[TwoAxisListPlot::dims,Dimensions[input]]];

(* plotranges = {horizontal,Subscript[vertical, 1],Subscript[vertical, 2]} *)
plotranges=MultiAxisListPlotRanges[OptionValue[PlotRange],input];

(* rescale to fit in {0,1} *)
gfxrange={0,1};
Do[
	mapfunc[\[FormalI]]=Evaluate[(#-plotranges[[1+\[FormalI],1]])/(plotranges[[1+\[FormalI],2]]-plotranges[[1+\[FormalI],1]]) (gfxrange[[2]]-gfxrange[[1]])+gfxrange[[1]]]&;
	mappeddata[\[FormalI]]={#[[1]],mapfunc[\[FormalI]][#[[\[FormalI]+1]]]}&/@input,
{\[FormalI],n}];

(* frame label *)
framelabel=If[Length[OptionValue[FrameLabel]]==3,
{OptionValue[FrameLabel][[2;;3]],{OptionValue[FrameLabel][[1]],None}},
OptionValue[FrameLabel]
];

(* output plot *)
ListPlot[
Table[mappeddata[\[FormalI]],{\[FormalI],n}],
Join[
{
Frame->True,
PlotRange->{First@plotranges,gfxrange},
FrameTicks->TwoAxisFrameTicks[OptionValue[FrameTicks],Table[mapfunc[\[FormalI]],{\[FormalI],n}],Rest@plotranges],
FrameStyle->TwoAxisFrameStyle[OptionValue[FrameStyle],OptionValue[PlotStyle]],
FrameLabel->framelabel,
PlotRangePadding->OptionValue[PlotRangePadding]
},
DeleteCases[Flatten[{opts}],PlotRange|FrameTicks|FrameStyle|PlotRangePadding->_]
]
]

]


Options[TwoAxisListPlot]=Join[
DeleteCases[Options[ListPlot],Frame|PlotRangePadding|PlotStyle->_],{
PlotRangePadding->{Scaled[.02],Scaled[.04]},
PlotStyle->ColorData[97,"ColorList"]
}];


TwoAxisPlot[funcs_List,vardomain_List,opts:OptionsPattern[]]:=Module[{
n=2,
plotranges,gfxrange,mapfunc,mappedfunc,
framestyle,framelabel
},

(* plotranges = {horizontal,Subscript[vertical, 1],Subscript[vertical, 2]} *)
plotranges=MultiAxisPlotRanges[OptionValue[PlotRange],funcs,vardomain];

(* rescale to fit in {0,1} *)
gfxrange={0,1};
Do[
	mapfunc[\[FormalI]]=Evaluate[(#-plotranges[[1+\[FormalI],1]])/(plotranges[[1+\[FormalI],2]]-plotranges[[1+\[FormalI],1]]) (gfxrange[[2]]-gfxrange[[1]])+gfxrange[[1]]]&;
	mappedfunc[\[FormalI]]=mapfunc[\[FormalI]][funcs[[\[FormalI]]]],
{\[FormalI],n}];

(* frame label *)
framelabel=If[Length[OptionValue[FrameLabel]]==3,
{OptionValue[FrameLabel][[2;;3]],{OptionValue[FrameLabel][[1]],None}},
OptionValue[FrameLabel]
];

(* output plot *)
Plot[
Evaluate@Table[mappedfunc[\[FormalI]],{\[FormalI],n}],vardomain,
Evaluate@Join[
{
Frame->True,
PlotRange->{First@plotranges,gfxrange},
FrameTicks->TwoAxisFrameTicks[OptionValue[FrameTicks],Table[mapfunc[\[FormalI]],{\[FormalI],n}],Rest@plotranges],
FrameStyle->TwoAxisFrameStyle[OptionValue[FrameStyle],OptionValue[PlotStyle]],
FrameLabel->framelabel,
PlotRangePadding->OptionValue[PlotRangePadding]
},
DeleteCases[Flatten[{opts}],PlotRange|FrameTicks|FrameStyle|PlotRangePadding->_]
]
]

]


Options[TwoAxisPlot]=Join[
DeleteCases[Options[Plot],Frame|PlotRangePadding|PlotStyle->_],{
PlotRangePadding->{Scaled[.02],Scaled[.04]},
PlotStyle->ColorData[97,"ColorList"]
}];


ColumnPlotRangePaddingMargin[option_,n_]:=Module[{plotrangepadding,margin,},
(* plotrangepadding *)
If[ListQ[option],
	Switch[option[[1]],
		Scaled[_],
		plotrangepadding={option[[1]],None},
		None|0,
		plotrangepadding={None,None}
	];
	Switch[option[[2]],
		Scaled[_],
		margin=2n First[option[[2]]],
		None|0,
		margin=0
	],
	Switch[option,
		Scaled[_],
		plotrangepadding={option,None};
		margin=2n First[option],
		None|0,
		plotrangepadding={None,None};
		margin=0
	]
];
(* output *)
{plotrangepadding,margin}
]


ColumnFrameTicks[option_,mapfunc_List,plotrange_List]:=Module[{frameticks,n=Length[mapfunc]},
If[ListQ[option],
	(* list input*)
	If[ListQ[option[[1]]],
	If[ListQ[option[[1,1]]],
	Do[frameticks[\[FormalI],Left]=If[MemberQ[{Automatic,True,All},option[[1,1,\[FormalI]]]],
	MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],
	Switch[Depth[option[[1,1,\[FormalI]]]],
	1,option[[1,1,\[FormalI]]],
	2,{mapfunc[\[FormalI]][#],#}&/@option[[1,1,\[FormalI]]],
	3,{mapfunc[\[FormalI]][#[[1]]],#[[2]]}&/@option[[1,1,\[FormalI]]]
	]
	],{\[FormalI],2}],
	Switch[option[[1,1]],
	Automatic|All|True,
	Do[frameticks[\[FormalI],Left]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],n}],
	None|_,
	Do[frameticks[\[FormalI],Left]={},{\[FormalI],n}]
	]
	];
	If[ListQ[option[[1,2]]],
	Do[frameticks[\[FormalI],Right]=Which[
	option[[1,2,\[FormalI]]]===Automatic,MapAt[mapfunc[[\[FormalI]]],Charting`ScaledFrameTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],
	MemberQ[{True,All},option[[1,2,\[FormalI]]]],MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],
	True,Switch[Depth[option[[1,2,\[FormalI]]]],
	1,option[[1,2,\[FormalI]]],
	2,{mapfunc[\[FormalI]][#],#}&/@option[[1,2,\[FormalI]]],
	3,{mapfunc[\[FormalI]][#[[1]]],#[[2]]}&/@option[[1,2,\[FormalI]]]
	]
	],{\[FormalI],2}],
	Switch[option[[1,2]],
	Automatic,
	Do[frameticks[\[FormalI],Right]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledFrameTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],n}],
	All|True,
	Do[frameticks[\[FormalI],Right]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],n}],
	None|_,
	Do[frameticks[\[FormalI],Right]=None,{\[FormalI],n}]
	]
	],
	Switch[option[[1]],
	Automatic,
	Do[frameticks[\[FormalI],Left]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],n}];
	Do[frameticks[\[FormalI],Right]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledFrameTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],n}],
	None|_,
	Do[frameticks[\[FormalI],\[FormalS]]=None,{\[FormalI],n},{\[FormalS],{Left,Right}}]
	]
	];
	If[ListQ[option[[2]]],
	frameticks[0,Bottom]=option[[2,1]];frameticks[0,Top]=option[[2,2]],
	frameticks[0,Bottom]=option[[2]];frameticks[0,Top]=option[[2]]
	],
	(* single input *)
	Switch[option,
	Automatic,
	Do[frameticks[0,\[FormalS]]=Automatic,{\[FormalS],{Bottom,Top}}];
	Do[frameticks[\[FormalI],Left]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],n}];
	Do[frameticks[\[FormalI],Right]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledFrameTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],n}],
	All,
	Do[frameticks[0,\[FormalS]]=All,{\[FormalS],{Bottom,Top}}];
	Do[frameticks[\[FormalI],\[FormalS]]=MapAt[mapfunc[[\[FormalI]]],Charting`ScaledTicks[{Identity,Identity}]@@plotrange[[\[FormalI]]],{All,1}],{\[FormalI],n},{\[FormalS],{Left,Right}}],
	None|_,
	Do[frameticks[0,\[FormalS]]=None,{\[FormalS],{Bottom,Top}}];
	Do[frameticks[\[FormalI],\[FormalS]]={},{\[FormalI],n},{\[FormalS],{Left,Right}}]
	]
];
(* output *)
{{Join@@Table[frameticks[\[FormalI],Left],{\[FormalI],n}],Join@@Table[frameticks[\[FormalI],Right],{\[FormalI],n}]},{frameticks[0,Bottom],frameticks[0,Top]}}
]


ColumnFrameLabel[option_,n_,opts:OptionsPattern[ColumnListPlot]]:=Module[{
framehorizlabel,framevertlabel,
autoopts,findimagepadding,findimagesize
},
(* frame label *)
If[ListQ[option\[And](Length[option]>0)],
	framehorizlabel=option[[1]];
	If[Length[option]==n+1,
		framevertlabel=option[[2;;]],
		framevertlabel=None
	],
	framehorizlabel=option;
	framevertlabel=None
];
(* find automatic options *)
autoopts=AbsoluteOptions[ListPlot[
{},
Join[
{
Frame->True,
FrameLabel->{framehorizlabel,If[ListQ[framevertlabel],Row[framevertlabel],framevertlabel]}
},
DeleteCases[Flatten[FilterRules[{opts},Options[ListPlot]]],FrameLabel->_]
]
]];
findimagepadding=Lookup[autoopts,ImagePadding];
findimagesize=Lookup[autoopts,ImageSize];
(* output *)
{framehorizlabel,If[ListQ[framevertlabel],Pane[
	Grid[{Reverse@framevertlabel},ItemSize->Scaled[1/n],Spacings->0],
	(findimagesize[[2]] -Total[findimagepadding[[2]]])],framevertlabel]}
]


ColumnSeparators[style_,xrange_List,padding_List,ylist_List,OptionsPattern[{"SeparatorTicks"->Automatic}]]:=Module[{padshift,seplines},
If[Not@MatchQ[style,None],
	(* separator lines *)
	padshift=(padding/.{Scaled[\[FormalX]_]:>(Subtract@@Reverse[xrange])\[FormalX],None->0});
	Switch[OptionValue["SeparatorTicks"],
		Automatic|All|True,
		seplines[y_]:=Translate[Line[{Scaled[Reverse[#[[3]]]],Scaled[-Reverse[#[[3]]]]}],{#[[1]],y}+padshift]&/@(Charting`ScaledFrameTicks[{Identity,Identity}]@@xrange),
		None,
		seplines[_]:={}
	];
	Join[
	{style},
	Table[InfiniteLine[{0,\[FormalY]},{1,0}],{\[FormalY],ylist}],
	Flatten[Table[seplines[\[FormalY]],{\[FormalY],ylist}],1]
	],
	{}
]
]


ColumnListPlot[input_List,opts:OptionsPattern[]]:=Module[{
n=Dimensions[input][[2]]-1,
plotranges,extplotrange,gfxrange,mapfunc,mappeddata,
framestyle,frameticks,framelabel,
plotrangepadding,margin,
seplines,padshift},

(* padding and margin *)
{plotrangepadding,margin}=ColumnPlotRangePaddingMargin[OptionValue[PlotRangePadding],n];

(* plotranges = {horizontal,Subscript[vertical, 1],Subscript[vertical, 2]} *)
plotranges=MultiAxisListPlotRanges[OptionValue[PlotRange],input];
(* extend plot range by margin *)
Do[
	With[{plotrangelength=Subtract@@Reverse[plotranges[[1+\[FormalI]]]]},
		extplotrange[\[FormalI]]=If[plotrangelength>0,
			{plotranges[[1+\[FormalI],1]]-margin*plotrangelength,plotranges[[1+\[FormalI],2]]+margin*plotrangelength},
			{0,2*plotranges[[1+\[FormalI],1]]}
		]
	],
{\[FormalI],n}];

(* rescale and shift *)
gfxrange[\[FormalI]_]:={n-\[FormalI],n-(\[FormalI]-1)};
Do[
	mapfunc[\[FormalI]]=Evaluate[(#-extplotrange[\[FormalI]][[1]])/(extplotrange[\[FormalI]][[2]]-extplotrange[\[FormalI]][[1]]) (gfxrange[\[FormalI]][[2]]-gfxrange[\[FormalI]][[1]])+gfxrange[\[FormalI]][[1]]]&;
	mappeddata[\[FormalI]]={#[[1]],mapfunc[\[FormalI]][#[[\[FormalI]+1]]]}&/@input,
{\[FormalI],n}];

(* frameticks, using plotranges instead of extplotrange to avoid collisions *)
frameticks=ColumnFrameTicks[OptionValue[FrameTicks],Table[mapfunc[\[FormalI]],{\[FormalI],n}],Rest@plotranges];
framelabel=ColumnFrameLabel[OptionValue[FrameLabel],n,Join[
	{
		PlotRange->{First@plotranges,{0,n}},
		FrameTicks->frameticks,
		PlotRangePadding->plotrangepadding
	},
	DeleteCases[Flatten[{opts}],PlotRange|FrameTicks|FrameLabel|PlotRangePadding|"Separators"|"SeparatorStyle"->_]
]];

(* output plot *)
ListPlot[
	Table[mappeddata[\[FormalI]],{\[FormalI],n}],
	Join[
		{
			Frame->True,
			PlotRange->{First@plotranges,{0,n}},
			FrameTicks->frameticks,
			PlotRangePadding->plotrangepadding,
			FrameLabel->framelabel,
			Epilog->ColumnSeparators[OptionValue["SeparatorStyle"],First@plotranges,plotrangepadding,Range[1,n-1],"SeparatorTicks"->OptionValue["SeparatorTicks"]]
		},
		DeleteCases[Flatten[{opts}],PlotRange|FrameTicks|FrameLabel|PlotRangePadding|"SeparatorStyle"|"SeparatorTicks"->_]
	]
]

];


Options[ColumnListPlot]=Join[
DeleteCases[Options[ListPlot],Frame|PlotRangePadding->_],{
PlotRangePadding->Scaled[0.02],
"SeparatorStyle"->GrayLevel[0.4],"SeparatorTicks"->Automatic
}];


ColumnPlot[funcs_List,vardomain_List,opts:OptionsPattern[]]:=Module[{
n=Length[funcs],
plotranges,extplotrange,gfxrange,mapfunc,mappedfunc,
framestyle,frameticks,framelabel,
plotrangepadding,margin,
seplines,padshift},

(* padding and margin *)
{plotrangepadding,margin}=ColumnPlotRangePaddingMargin[OptionValue[PlotRangePadding],n];

(* plotranges = {horizontal,Subscript[vertical, 1],Subscript[vertical, 2]} *)
plotranges=MultiAxisPlotRanges[OptionValue[PlotRange],funcs,vardomain];
(* extend plot range by margin *)
Do[
	With[{plotrangelength=Subtract@@Reverse[plotranges[[1+\[FormalI]]]]},
		extplotrange[\[FormalI]]=If[plotrangelength>0,
			{plotranges[[1+\[FormalI],1]]-margin*plotrangelength,plotranges[[1+\[FormalI],2]]+margin*plotrangelength},
			{0,2*plotranges[[1+\[FormalI],1]]}
		]
	],
{\[FormalI],n}];

(* rescale and shift *)
gfxrange[\[FormalI]_]:={n-\[FormalI],n-(\[FormalI]-1)};
Do[
	mapfunc[\[FormalI]]=Evaluate[(#-extplotrange[\[FormalI]][[1]])/(extplotrange[\[FormalI]][[2]]-extplotrange[\[FormalI]][[1]]) (gfxrange[\[FormalI]][[2]]-gfxrange[\[FormalI]][[1]])+gfxrange[\[FormalI]][[1]]]&;
	mappedfunc[\[FormalI]]=mapfunc[\[FormalI]][funcs[[\[FormalI]]]],
{\[FormalI],n}];

(* frameticks, using plotranges instead of extplotrange to avoid collisions *)
frameticks=ColumnFrameTicks[OptionValue[FrameTicks],Table[mapfunc[\[FormalI]],{\[FormalI],n}],Rest@plotranges];
framelabel=ColumnFrameLabel[OptionValue[FrameLabel],n,Join[
	{
		PlotRange->{First@plotranges,{0,n}},
		FrameTicks->frameticks,
		PlotRangePadding->plotrangepadding
	},
	DeleteCases[Flatten[{opts}],PlotRange|FrameTicks|FrameLabel|PlotRangePadding|"Separators"|"SeparatorStyle"->_]
]];

(* output plot *)
Plot[
	Evaluate@Table[mappedfunc[\[FormalI]],{\[FormalI],n}],vardomain,
	Evaluate@Join[
		{
			Frame->True,
			PlotRange->{First@plotranges,{0,n}},
			FrameTicks->frameticks,
			PlotRangePadding->plotrangepadding,
			FrameLabel->framelabel,
			Epilog->ColumnSeparators[OptionValue["SeparatorStyle"],First@plotranges,plotrangepadding,Range[1,n-1],"SeparatorTicks"->OptionValue["SeparatorTicks"]]
		},
		DeleteCases[Flatten[{opts}],PlotRange|FrameTicks|FrameLabel|PlotRangePadding|"SeparatorStyle"|"SeparatorTicks"->_]
	]
]
];


Options[ColumnPlot]=Join[
DeleteCases[Options[Plot],Frame|PlotRangePadding->_],{
PlotRangePadding->Scaled[0.02],
"SeparatorStyle"->GrayLevel[0.4],"SeparatorTicks"->Automatic
}];


(* ::Subsection::Closed:: *)
(*Old*)


MultiAxisPlot[data_List (*{{Subscript[x, 1],Subscript[a, 1],Subscript[b, 1],...},{Subscript[x, 2],Subscript[a, 2],Subscript[b, 2],...},...}*),OptionsPattern[]]:=Module[{\[FormalN],min,max,plotrangemult,plotrange,imagesize,imageaspect,axesaspect,vertpad,axeslabels,plotstyles,joined,interporder,ticks,plotlabel},
\[FormalN]=Length[data[[1]]]-1;

axesaspect=5;
plotrangemult=1.1;
imagesize=OptionValue[ImageSize];
imageaspect=OptionValue[AspectRatio];
vertpad=OptionValue[ImagePadding];
joined=OptionValue[Joined];
interporder=OptionValue[InterpolationOrder];
ticks=OptionValue[Ticks];
plotlabel=OptionValue[PlotLabel];

If[Head@OptionValue[AxesLabel]===List,axeslabels=OptionValue[AxesLabel],axeslabels=ConstantArray["?",\[FormalN]+1]];
If[Head@OptionValue[PlotStyle]===List,plotstyles=OptionValue[PlotStyle],plotstyles=Table[Hue[\[FormalI]/\[FormalN]],{\[FormalI],\[FormalN]}]];
Do[
min[\[FormalI]]=Min[data[[All,1+\[FormalI]]]];max[\[FormalI]]=Max[data[[All,1+\[FormalI]]]];
plotrange[\[FormalI]]={(min[\[FormalI]]+max[\[FormalI]])/2-(max[\[FormalI]]-min[\[FormalI]])/2 plotrangemult,(min[\[FormalI]]+max[\[FormalI]])/2+(max[\[FormalI]]-min[\[FormalI]])/2 plotrangemult};,{\[FormalI],\[FormalN]}];

Row[
Table[Graphics[{},Axes->{False,True},PlotRange->{{0,1},plotrange[\[FormalI]]},AxesOrigin->{0.7,0},AspectRatio->axesaspect,ImageSize->imagesize imageaspect/axesaspect,ImagePadding->{{0,0},{vertpad,vertpad}},ImageMargins->0,PlotRangePadding->0,BaselinePosition->Center,AxesLabel->Style[axeslabels[[\[FormalI]+1]],Black],AxesStyle->plotstyles[[\[FormalI]]],TicksStyle->Black],{\[FormalI],\[FormalN]}]
~Join~{Overlay[
Table[ListPlot[data[[All,{1,1+\[FormalI]}]],ImageSize->imagesize+20,AspectRatio->imageaspect,ImageMargins->0,PlotRange->plotrange[\[FormalI]],ImagePadding->{{10,vertpad},{vertpad,vertpad}},BaselinePosition->Center,Axes->{True,False},AxesOrigin->{Automatic,plotrange[\[FormalI]][[1]]},AxesLabel->{axeslabels[[1]],None},PlotStyle->plotstyles[[\[FormalI]]],Joined->joined,InterpolationOrder->interporder,Epilog->If[joined,{PointSize[Medium],Point[data[[All,{1,1+\[FormalI]}]]]},{}],Ticks->{ticks,None},PlotLabel->plotlabel],{\[FormalI],\[FormalN]}],
BaselinePosition->Center
]}]
];


Options[MultiAxisPlot]={ImageSize->450,AspectRatio->1/GoldenRatio,ImagePadding->20,AxesLabel->Automatic,PlotStyle->Automatic,Joined->False,InterpolationOrder->1,Ticks->Automatic,PlotLabel->None};


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
